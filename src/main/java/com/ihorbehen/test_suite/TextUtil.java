package com.ihorbehen.test_suite;

public class TextUtil {

    private String text;

    public TextUtil(String text){
        this.text = text;
    }

    public String printText(){
        System.out.println(text);
        return text;
    }

    public String additionText(){
        text = "Java" + text;
        System.out.println(text);
        return text;
    }
}
