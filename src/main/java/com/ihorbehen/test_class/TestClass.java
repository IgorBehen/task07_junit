package com.ihorbehen.test_class;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;


class TestClass {
    private static final Logger log = LogManager.getLogger(TestClass.class);

    protected static final int groupID = 1;
    private int a;
    private int b;
    private  int d;
    private String s = "Numbers: ";
    static String p = null;
    static final String obj1= "It`s a addThreeNumbers() method testing...";
    protected static final String obj2="It`s a testShowInfo() method testing...";
    int[] arrary1 = { 1, 2, 3 };
    int[] arrary2 = { 1, 2, 3 };



    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }

    public int getB() {
        return b;
    }

    public void setB(int b) {
        this.b = b;
    }

    public int getD() {
        return d;
    }

    public void setD(int d) {
        this.d = d;
    }

    public int addThreeNumbers(int a, int b, int c) {
        return a + b + c;
    }

    public String showInfo() {
        String text = s + a + ", " + b;
        log.info(text);
        return text;
    }

    public double multDiv(double a, double b, double c) {
        return a * b / c;
    }

    public String disabled(){
        return null;
    }
}
