package com.ihorbehen.minesweeper;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Minesweeper {
    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numberN = 0;
        int numberM = 0;
        int numberP = 0;
        /*Boolean*/
        int[][] array;  /*= new Boolean[numberN][numberM];*/

        try {
            System.out.println("Enter the number(N) of first-party items in the box:");
            numberN = (int) scan.nextInt();
            System.out.println("Enter the number(M) of first-party items in the box:");
            numberM = (int) scan.nextInt();
            System.out.println("Enter the number of bombs(P):");
            numberP = (int) scan.nextInt();
        } catch (InputMismatchException e) {
            System.out.println("Please enter the correct value!");
        }
        array = new int /*Boolean*/[numberN][numberM];
        System.out.println("Print board:");
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array[i].length; j++) {
                array[i][j] = j;
            }
        }
        for (int i = 0; i < array.length; i++) {
            System.out.print((i + 1) + "\t");
        }
        System.out.println();
        for (int i = 0; i < array.length * 4; i++) {
            System.out.print("@");
        }
        System.out.println();
        for (int i = 0; i < array.length; i++) {
            if (i > 0) {
                System.out.println(i);
            }
            for (int j = 0; j < array[i].length; j++) {
                System.out.print(array[i][j] + "\t");
            }
            System.out.print("@ ");
        }
        System.out.println(array.length);
        System.out.println("The bottom of the board.");

        /**
         * Not finished. In process...
         */
    }
}
