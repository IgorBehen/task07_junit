package com.ihorbehen.longest_plateau;

public class LongestPlateau {
    int plateauArray[] = new int[300];
    int count = 0;
    int random;

    public void randomArrayInit() {
        for (int i = 0; i < plateauArray.length; i++) {
            random = (int) (Math.random() * 101);
            plateauArray[i] = random;
        }
    }

    void arrayOnConsole() {
        for (int j = 0; j < plateauArray.length; j++) {
            count++;
            System.out.println(" " + (j + 1) + ") " + plateauArray[j]);
        }
        System.out.println("The number of elements in the array is " + count);
    }

    public static void main(String[] args) {
        LongestPlateau longestPlateau = new LongestPlateau();
        longestPlateau.randomArrayInit();
        longestPlateau.arrayOnConsole();

        int firstPositionOfPlateau = 0;
        int lenghtOfPlateau = 0;
        int firstPositionOfPlateauCandidate = 0;
        int lenghtOfPlateauCandidate = 0;

        for (int i = 0; i < longestPlateau.plateauArray.length; i++) {
            if (i < longestPlateau.plateauArray.length - 1) {
                if (longestPlateau.plateauArray[i] == longestPlateau.plateauArray[i + 1]) {
                    firstPositionOfPlateauCandidate = i;
                    lenghtOfPlateauCandidate++;
                    if (lenghtOfPlateauCandidate > 1) {
                        firstPositionOfPlateauCandidate = i - (lenghtOfPlateauCandidate - 1);
                    }
                } else if (longestPlateau.plateauArray[i] > longestPlateau.plateauArray[i + 1]) {
                    if (lenghtOfPlateauCandidate > 1 && lenghtOfPlateauCandidate > lenghtOfPlateau) {
                        firstPositionOfPlateau = firstPositionOfPlateauCandidate;
                        lenghtOfPlateau = lenghtOfPlateauCandidate;
                    }
                    lenghtOfPlateauCandidate = 0;
                    firstPositionOfPlateauCandidate = i + 1;
                    lenghtOfPlateauCandidate++;
                } else if (longestPlateau.plateauArray[i] < longestPlateau.plateauArray[i + 1]) {
                    if (lenghtOfPlateauCandidate > 1 && lenghtOfPlateauCandidate > lenghtOfPlateau) {
                        firstPositionOfPlateau = firstPositionOfPlateauCandidate;
                        lenghtOfPlateau = lenghtOfPlateauCandidate;
                    }
                    firstPositionOfPlateauCandidate = 0;
                    lenghtOfPlateauCandidate = 0;
                }
            }
        }
        if (firstPositionOfPlateau == 0 && lenghtOfPlateau <= 1) {
            System.out.println("There is no plateau in this sequence.");
        } else {
            System.out.println("Plateau of " + (firstPositionOfPlateau + 2) + " positions and length " + (lenghtOfPlateau + 1) + " is the longest");
        }
    }
}
