package com.ihorbehen.test_class;

import org.junit.jupiter.api.*;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@ExtendWith(MockitoExtension.class)
class TestClassTest {
    private TestClass testClass;
    private TestClass testClass1 = new TestClass();

    @BeforeAll
    static void testsStarted() {
        System.out.println("Tests - started!");
    }

    @BeforeEach
    void setup() {
        testClass = new TestClass();
        System.out.println("The test has begun...");
    }

    @AfterAll
    static void testsFinished() {
        System.out.println("Tests - finished!");
    }

    @DisplayName("Test for objectNotNull")
    @Test
    void ObjectNotNullTest() {
        assertNotNull(testClass);
        System.out.println("objectNotNull() is passed!");
    }

    @Test
    void valueIsNullTest() {
        assertNull(TestClass.p);
        System.out.println("valueIsNullTest() is passed!");
    }

    @Test
    void ifArrayEqualsTest() {
        assertArrayEquals(testClass.arrary1, testClass.arrary2);
    }

    @Test
    void notSameObjectsTest() {
        assertNotSame(testClass, testClass1);
    }

    @Test
    void AddThreeNumbersTest() {
        int sum = testClass.addThreeNumbers(1, 2, 3);
        assertTrue(sum == 6);
        System.out.println(TestClass.obj1);
    }

    @RepeatedTest(3)
    void repeatedTest(TestInfo testInfo) {
        System.out.println("Executing repeated test");
        assertEquals(2, Math.addExact(1, 1));
        assertEquals(4, Math.addExact(2, 2));
        assertEquals(10, Math.addExact(5, 5));
    }

    @Test
    void ShowInfoTest() {
        assertNotNull(testClass.showInfo());
        System.out.println(TestClass.obj2);
    }

    @Test
    void MultDivTest() {
        assertEquals(10, testClass.multDiv(5, 6, 3));
        System.out.println("multDiv() is passed!");

    }

    @Disabled
    @Test
    void TestDisabledTest() {
        assertEquals(1, 2);
    }

    @Test
    void setVoidDTest(){
        testClass.setD(155);
        assertEquals(155, testClass.getD());
        System.out.println("The test of 'void' method is pased!");
    }

    @Mock
    private List<String> listOfMocks;

    @Test
    void testListOfMocksAddLineToList() {
        String textLine = "line of text";
        listOfMocks.add(textLine);
        verify(listOfMocks).add(textLine);
        System.out.println("The testListOfMocksAddLineToList() passed!");
    }

    @Test
    void testListOfMocksAddManyLinesToList() {
        String textLines = "one of the strings";
        listOfMocks.add(textLines);
        listOfMocks.add(textLines);
        listOfMocks.add(textLines);
        verify(listOfMocks, times(3)).add(textLines);
        System.out.println("The testListOfMocksAddManyLinesToList() passed!");
    }
}