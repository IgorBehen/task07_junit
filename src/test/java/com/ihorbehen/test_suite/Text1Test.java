package com.ihorbehen.test_suite;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class Text1Test {

    String text = "Hello";
    TextUtil textUtil = new TextUtil(text);

   @Test
    public void testPrintText() {
        System.out.println("Inside testPrintText()");
        assertEquals(text, textUtil.printText());
    }
}
