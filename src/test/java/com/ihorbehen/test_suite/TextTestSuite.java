package com.ihorbehen.test_suite;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)

@Suite.SuiteClasses({
        Text1Test.class,
        Text2Test.class
})

public class TextTestSuite {
}
